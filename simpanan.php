<?php
    require($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <!-- ##### Breadcrumb Area Start ##### -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(/mag/img/bg-img/1.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>SIMPANAN</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="mag-breadcrumb py-5">
    </div>


    <!-- ##### Archive Post Area Start ##### -->
    
    <div class="archive-post-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-xl-8">
                    <div class="post-details-content bg-white mb-30 p-30 box-shadow">
                        <div class="blog-content">
                            <h4 class="post-title text-center">SIMPANAN SAHAM</h4>
                            <!-- Post Meta -->
                            <ul style="list-style-type:disc;">
                                <li>Setoran Awal RP. 50.000, bisa di stor dan ditarik setiap hari.</li>
                                <li>Bunga 4%/tahun (dapat berubah sewaktu-waktu sesuai dengan harga pasar) dihitung stiap hari dan dibayar pada akhir bulan.</li>
                                <li>Bunga SIBUHAR langsung didebetkan pada buku SIBUHAR anggota pada setiap ahkir bulan.</li>
                                <li>Saldo minimal Rp. 50.000.</li>
                                <li>Biaya Penggantian buku SIBUHAR Rp. 6.000.</li>
                                <li>Simpanan ini tidak di lindungi oleh DAPERMA</li> 
                                <li>Tidak dikenakan biaya administrasi bulanan.</li>
                            </ul>   

                            
                            <!-- Like Dislike Share -->
                            <div class="like-dislike-share my-5">
                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Share on Facebook</a>
                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i> Share on Twitter</a>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                    <div class="sidebar-area bg-white mb-30 box-shadow">
                        <!-- Sidebar Widget -->
                        <div class="single-sidebar-widget p-30">
                            <!-- Section Title -->
                            <div class="section-heading">
                                <h5>Simpanan</h5>
                            </div>

                            <!-- Catagory Widget -->
                            <ul class="catagory-widgets">
                                <?php
                                    $simpanan_list = [
                                        [ 'text' => 'SAHAM', 'url' => '/saham.php' ],
                                        [ 'text' => 'SIBUHAR', 'url' => '/sibuhar.php' ],
                                        [ 'text' => 'SIPANDIK', 'url' => '/sipandik.php' ],
                                        [ 'text' => 'SISUKA', 'url' => '/sisuka.php' ],
                                        [ 'text' => 'SIMAPAN BULANAN', 'url' => '/simapan_bln.php' ],
                                        [ 'text' => 'SIMAPAN KONTRAK', 'url' => '/simapan_ktr.php' ],
                                        [ 'text' => 'SIMPATI', 'url' => '/simpati.php' ],
                                        [ 'text' => 'SKA', 'url' => '/ska.php' ]
                                    ];
                                    foreach ($simpanan_list as $item) { ?>
                                        <li>
                                            <a href="<?= $item['url'] ?>">
                                                <span>
                                                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                                        <?= $item['text']; ?>
                                                </span>
                                            </a>
                                        </li>
                                    <?php }
                                ?>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
</body>

</html>