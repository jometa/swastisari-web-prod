<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="/js/jquery.counterup.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/css/slick-theme.css"/>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <!-- ##### Hero Area Start ##### -->
    <div class="hero-area owl-carousel">
        <!-- Single Blog Post -->
        <div class="hero-blog-post bg-img bg-overlay" style="background-image: url(/mag/img/bg-img/3.jpg);">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <!-- Post Contetnt -->
                        <div class="post-content text-center">
                            <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                <a href="#">MAY 8, 2018</a>
                                <a href="archive.html">lifestyle</a>
                            </div>
                            <a href="video-post.html" class="post-title" data-animation="fadeInUp" data-delay="300ms">Party Jokes Startling But Unnecessary</a>
                            <a href="video-post.html" class="video-play" data-animation="bounceIn" data-delay="500ms"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="hero-blog-post bg-img bg-overlay" style="background-image: url(img/slide2.png);">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <!-- Post Contetnt -->
                        <div class="post-content text-center">
                            <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                <a href="#">MAY 8, 2018</a>
                                <a href="archive.html">lifestyle</a>
                            </div>
                            <a href="video-post.html" class="post-title" data-animation="fadeInUp" data-delay="300ms">Party Jokes Startling But Unnecessary</a>
                            <a href="video-post.html" class="video-play" data-animation="bounceIn" data-delay="500ms"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Hero Area End ##### -->

    <div class="perkembangan-jumbotron jumbotron-fluid">
        <div class="container text-center d-flex flex-column justify-content-center align-items-center h-100" style="padding-top: 32px; padding-bottom: 32px;">
            <div class="section-heading">
                <h5>Statistik Perkembangan</h5>
            </div>
            <div class="py-0 mb-4 pri-bg" style="width: 100%; height: 1px;"></div>
            <div class="row w-100">
                <?php
                    $query_result = $db->query("SELECT SUM(kantor_cbg.jmlh_anggota) as total_anggota FROM kantor_cbg");
                    if (!$query_result) {
                        exit('error load data');
                    }

                    $query_total_cabang = $db->query("SELECT COUNT(kantor_cbg.id) as total_cabang FROM kantor_cbg");
                    if (!$query_result) {
                        exit('error load data');
                    }

                    $query_total_kas = $db->query("SELECT COUNT(kantor_kas.id) as total_kas FROM kantor_kas");
                    if (!$query_result) {
                        exit('error load data');
                    }

                    $total_anggota = $query_result->fetch_assoc()['total_anggota'];
                    $total_kas = $query_total_kas->fetch_assoc()['total_kas'];
                    $total_cabang = $query_total_cabang->fetch_assoc()['total_cabang'];

                ?>
                <div class="col-md-4 col-xs-12">
                    <span class="font-weight-bold pri-text">Total Anggota</span>
                    <div class="font-weight-bold display-4 counter"><?= $total_anggota ?></div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <span class="font-weight-bold pri-text">Total Cabang</span>
                    <div class="font-weight-bold display-4 counter"><?= $total_cabang ?></div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <span class="font-weight-bold pri-text">Total Kas</span>
                    <div class="font-weight-bold display-4 counter"><?= $total_kas ?></div>
                </div>
            </div>
        </div>
    </div>

    <div class="feature jumbotron-fluid bg-white">
        <div class="container text-center d-flex flex-column justify-content-center align-items-center h-100" style="padding-top: 32px; padding-bottom: 32px;">
            <div class="section-heading">
                <h5>Layanan</h5>
            </div>
            <div class="py-0 mb-4 bg-danger" style="width: 100%; height: 1px;"></div>
            <div class="row w-100">
                <?php 
                    $layanan_menu_list = [
                        [ 'text' => 'Simpanan', 'url' => '/simpanan.php', 'image' => '/mag/img/bg-img/1.jpg' ],
                        [ 'text' => 'Pinjaman', 'url' => '/pinjaman.php', 'image' => '/mag/img/bg-img/2.jpg' ],
                        [ 'text' => 'ATM', 'url' => '/atm.php', 'image' => '/mag/img/bg-img/3.jpg' ],
                        [ 'text' => 'Keanggotaan', 'url' => '/keanggotaan.php', 'image' => '/mag/img/bg-img/4.jpg' ]
                    ];

                    foreach ($layanan_menu_list as $item) { ?>
                        <div class="col-md-3 col-xs-12">
                            <a 
                                class="single-sidebar-widget d-flex justify-content-center align-items-center"
                                style="background-image: url(<?= $item['image']; ?>)"
                                href="<?= $item['url'] ?>"
                            >
                                <span><?= $item['text'] ?></h4>
                            </a>
                        </div>
                    <?php }
                ?>
            </div>
        </div>
    </div>

    <div class="jumbotron jumbotron-fluid " style="margin-bottom: 0;">
        <div class="container text-center d-flex flex-column justify-content-center align-items-center h-100">
            <p class="text-white">Selamat Datang Di Website.</p>
            <h1 class="text-white font-weight-bold" style="font-size: 2.4rem">KOPDIT SWASTISARI</h1>
        </div>
    </div>

    <!-- ##### Mag Posts Area Start ##### -->
    <section class="container">

        <div class="row">
            <div class="col-md-8">
                <div class="mag-posts-content mt-30 mb-30 box-shadow">

                    <div class="feature-video-posts mb-30" style="height: 400px;">
                        <div class="hero-area owl-carousel mb-30" style="height: 400px; overflow-y: hidden;">
                            <!-- Single Blog Post -->
                            <div class="hero-blog-post bg-img bg-overlay" style="background-color: #1474c9;">
                                <div class="container h-100">
                                    <div class="row align-items-center">
                                        <div class="col-12">
                                            <!-- Post Contetnt -->
                                            <div class="post-content text-center">
                                                <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                                    <a href="#">MAY 8, 2018</a>
                                                    <a href="archive.html">lifestyle</a>
                                                </div>
                                                <a href="video-post.html" class="post-title" data-animation="fadeInUp" data-delay="300ms">Party Jokes Startling But Unnecessary</a>
                                                <a href="video-post.html" class="video-play" data-animation="bounceIn" data-delay="500ms"><i class="fa fa-play"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Blog Post -->
                            <div class="hero-blog-post bg-img bg-overlay" style="background-color: #1474c9;">
                                <div class="container h-100">
                                    <div class="row align-items-center">
                                        <div class="col-12">
                                            <!-- Post Contetnt -->
                                            <div class="post-content text-center">
                                                <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                                    <a href="#">MAY 8, 2018</a>
                                                    <a href="archive.html">lifestyle</a>
                                                </div>
                                                <a href="video-post.html" class="post-title" data-animation="fadeInUp" data-delay="300ms">Party Jokes Startling But Unnecessary</a>
                                                <a href="video-post.html" class="video-play" data-animation="bounceIn" data-delay="500ms"><i class="fa fa-play"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="feature-video-posts mb-30 p-3">
                        <!-- Section Title -->
                        <div class="section-heading">
                            <h5>Berita</h5>
                        </div>

                        <div class="featured-video-posts">
                            <div class="row">
                                

                                <div class="col-12 col-lg-12">
                                    <!-- Featured Video Posts Slide -->
                                    <div class="featured-video-posts-slide owl-carousel">

                                        <?php
                                        $slide_berita_query = $db->query("
                                            SELECT b.id, b.judul, b.foto, b.tanggal_buat
                                                FROM berita b
                                                LEFT JOIN berita_tag bt ON b.id = bt.id_berita
                                                LEFT JOIN tag ON bt.id_tag = tag.id
                                                WHERE tag.nama = 'berita'
                                                ORDER BY b.tanggal_buat DESC");
                                        $berita_for_tag = $slide_berita_query->fetch_all(MYSQLI_ASSOC);  ?>

                                        <div class="single--slide">
                                            <?php
                                                foreach ($berita_for_tag as $berita) { ?>
                                                    <div class="single-blog-post d-flex style-3">
                                                        <div class="post-thumbnail">
                                                            <img src="<?= $berita['foto'] ?>" alt="">
                                                        </div>
                                                        <div class="post-content" style="display: flex; align-items: start; flex-direction: column; justify-content: start;">
                                                            <a href="/berita-detail.php?id=<?= $berita['id'] ?>" class="post-title ma-0 d-block" style="; margin-bottom: 0; "><?=$berita['judul']?></a>
                                                            <span class="pri-text" style="font-size: 12px;"><?= $berita['tanggal_buat'] ?></span>
                                                        </div>
                                                    </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="feature-video-posts mb-30 p-3" style="margin-top:60px;">
                        <!-- Section Title -->
                        <div class="section-heading">
                            <h5>Pengumuman</h5>
                        </div>

                        <div class="featured-video-posts">
                            <div class="row">
                                

                                <div class="col-12 col-lg-12">
                                    <!-- Featured Video Posts Slide -->
                                    <div class="featured-video-posts-slide owl-carousel">

                                        <?php
                                        $slide_berita_query = $db->query("
                                            SELECT b.id, b.judul, b.foto, b.tanggal_buat
                                                FROM berita b
                                                LEFT JOIN berita_tag bt ON b.id = bt.id_berita
                                                LEFT JOIN tag ON bt.id_tag = tag.id
                                                WHERE tag.nama = 'pengumuman'
                                                ORDER BY b.tanggal_buat DESC");
                                        $berita_for_tag = $slide_berita_query->fetch_all(MYSQLI_ASSOC);  ?>

                                        <div class="single--slide">
                                            <?php
                                                foreach ($berita_for_tag as $berita) { ?>
                                                    <div class="single-blog-post d-flex style-3">
                                                        <div class="post-thumbnail">
                                                            <img src="<?= $berita['foto'] ?>" alt="">
                                                        </div>
                                                        <div class="post-content" style="display: flex; align-items: start; flex-direction: column; justify-content: start;">
                                                            <a href="/berita-detail.php?id=<?= $berita['id'] ?>" class="post-title ma-0 d-block" style="; margin-bottom: 0; "><?=$berita['judul']?></a>
                                                            <span class="pri-text" style="font-size: 12px;"><?= $berita['tanggal_buat'] ?></span>
                                                        </div>
                                                    </div>
                                            <?php } ?>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Most Viewed Videos -->
                    <div class="most-viewed-videos mb-30 p-3" style="margin-top:60px;">
                        <!-- Section Title -->
                        <div class="section-heading">
                            <h5>Video News</h5>
                        </div>

                        <div class="most-viewed-videos-slide owl-carousel">

                        <?php
                            $slide_berita_query = $db->query("
                                SELECT b.id, b.video_id, b.video_title
                                    FROM video_news b
                                    LEFT JOIN video_news_tag bt ON b.id = bt.id_video_news
                                    LEFT JOIN tag ON bt.id_tag = tag.id
                                    WHERE tag.nama = 'berita'");
                            $berita_for_tag = $slide_berita_query->fetch_all(MYSQLI_ASSOC);
                        
                            foreach ($berita_for_tag as $item) { ?>
                            <div class="single-blog-post style-4">
                                <div class="post-thumbnail">
                                    <img src="https://img.youtube.com/vi/<?= $item['video_id'] ?>/hqdefault.jpg" alt="">
                                    <a href="https://www.youtube.com/watch?v=<?= $item['video_id'] ?>" class="video-play"><i class="fa fa-play"></i></a>
                                </div>
                                <div class="post-content">
                                    <a title="<?= $item['video_title'] ?>" href="single-post.html" class="post-title">iNews NTT - 120 Calon Karyawan Swastisari...</a>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="post-sidebar-area bg-white mt-30 mb-30 box-shadow">
                    <?php include $_SERVER['DOCUMENT_ROOT'] . '/common-snippets/right-side-overview.php'; ?>
                </div>
            </div>
        </div>

        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Right Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
    </section>

    <section class="jumbotron-fluid bg-white d-none d-sm-block">
        <div class="container text-center d-flex flex-column justify-content-center align-items-center" style="padding-top: 32px; padding-bottom: 32px;">
            <div class="section-heading">
                <h5>Testimoni Anggota</h5>
            </div>
            <div class="row w-100 testimoni-slider">
                <?php
                    $slide_testimoni_query = $db->query("
                    SELECT b.id, b.judul, b.foto, b.tanggal_buat
                        FROM berita b
                        LEFT JOIN berita_tag bt ON b.id = bt.id_berita
                        LEFT JOIN tag ON bt.id_tag = tag.id
                        WHERE tag.nama = 'testimoni'
                      ORDER BY b.tanggal_buat DESC
                      LIMIT 6");
                    $testimoni_items = $slide_testimoni_query->fetch_all(MYSQLI_ASSOC); 

                    foreach ($testimoni_items as $item) { ?>
                        <div>
                            <a 
                                class="testimoni-item d-flex justify-content-center align-items-center"
                                style="background-image: url(<?= $item['foto']; ?>)"
                                href="/berita-detail.php?id=<?= $item['url'] ?>"
                            >
                                <span><?= $item['judul'] ?></h4>
                            </a>
                        </div>
                    <?php }
                ?>
            </div>
        </div>
    </section>

    <!-- ##### Mag Posts Area End ##### -->


    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript">
        $('.counter').counterUp();
        console.log($('.testimoni-slider').slick);
        $(document).ready(function(){
            $('.testimoni-slider').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3
            });
        });        
    </script>

</body>

</html>