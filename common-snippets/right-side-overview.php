<?php
	 require_once($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');

	 $query_result = $db->query("
	 	select cbg.id, cbg.nama, count(kas.id) as total_kas FROM
		kantor_cbg cbg
	    LEFT JOIN kantor_kas kas ON kas.id_kantor_cbg = cbg.id
	    GROUP BY cbg.id
	    ORDER BY total_kas DESC
	 ");

	 if (!$query_result) {
	 	exit('Gagal mengambil data cabang dan kas');
	 }

	 $items = $query_result->fetch_all(MYSQLI_ASSOC);
?>


    <!-- Sidebar Widget -->
    <div class="single-sidebar-widget p-30">
        <!-- Social Followers Info -->
        <div class="social-followers-info">
            <!-- Facebook -->
            <a href="#" class="facebook-fans"><i class="fa fa-facebook"></i> 4,360 <span>Fans</span></a>
            <!-- Twitter -->
            <a href="#" class="twitter-followers"><i class="fa fa-twitter"></i> 3,280 <span>Followers</span></a>
            <!-- YouTube -->
            <a href="#" class="youtube-subscribers"><i class="fa fa-youtube"></i> 1250 <span>Subscribers</span></a>
            <!-- Google -->
            <a href="#" class="google-followers"><i class="fa fa-google-plus"></i> 4,230 <span>Followers</span></a>
        </div>
    </div>

    <!-- Sidebar Widget -->
    <div class="single-sidebar-widget p-30">
        <!-- Section Title -->
        <div class="section-heading">
            <h5>Jumlah Kantor Cabang</h5>
        </div>

        <!-- Catagory Widget -->
        <ul class="catagory-widgets">
        	<?php
        		foreach ($items as $item) { ?>
        			<li>
        				<a href="/cabang-detail.php?id=<?= $item['id'] ?>">
        					<span>
        						<i class="fa fa-angle-double-right" aria-hidden="true"></i>
        							<?= $item['nama']; ?>
        					</span>
        					<span><?= $item['total_kas']; ?></span>
        				</a>
        			</li>
        		<?php }
        	?>
        </ul>
    </div>
