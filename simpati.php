<!DOCTYPE html>
<html lang="en">

<head>
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(/mag/img/bg-img/49.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>SYARAT KEANGGOTAAN</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div class="mag-breadcrumb py-5">
    </div>

    <!-- ##### Post Details Area Start ##### -->
    <section class="post-details-area">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Post Details Content Area -->
                <div class="col-12 col-xl-8">
                    <div class="post-details-content bg-white mb-30 p-30 box-shadow">
                        <div class="blog-content">
                            <h4 class="post-title text-center">SIMPANAN TITIPAN (SIMPATI)</h4>
                            <!-- Post Meta -->
                            
                            <p>Simpanan titipan adalah salah satu bentuk simpanan non saham, yang dibuka pada saat pencairan pinjaman.(masuk dalam poljak pinjaman)</p>
                            <p>Simpanan sebesar 1 kali (1x) angsuran pinjaman</p>
                            <p>Tujuan memperbesar simpanan anggota</p>
                            <p>Simpanan boleh ditarik jika pinjamannya sudah lunas dengan saldo minimal Rp. 100.000.</p>
                            <p>Bunga dihitung setiap bulan dan langsung dibukukan untuk menambah simpanan.</p>
                            <p>Dijadikan jaminan atas pinjaman.</p>
                            <p>Balas Jasa simpanan 5%/tahun.</p>
                            
                                

                            
                            <!-- Like Dislike Share -->
                            <div class="like-dislike-share my-5">
                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Share on Facebook</a>
                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i> Share on Twitter</a>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- ##### Post Details Area End ##### -->

    
    <!-- ##### Footer Area End ##### -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <?php
      include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
</body>

</html>