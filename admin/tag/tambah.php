<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');

    // Handle form data
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {


        // Set default picture for cabang.
        $picture_url = '/uploads/default-cabang.jpg';


        $konten = $db->real_escape_string($_POST['konten']);
        $insert_str = "
            INSERT INTO tag
                (nama)
            VALUES
                ('$_POST[nama]')
        ";
        $insert_result = $db->query($insert_str);

        if ($insert_result) { ?>
            <script>
                window.location = '/admin/tag/list.php';
            </script> 
        <?php } else {
                echo $insert_str;
                exit('Gagal mengupload data tag');
        }
    }
?>

<?php 
    require($_SERVER['DOCUMENT_ROOT'] . '/admin/templates/layout.php');
?>

<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/head.snip.html'; ?>
    <link href="/admin/assets/libs/css/quill.snow.css" rel="stylesheet"/>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/dash-head.snip.html'; ?>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <?php templ_left_nav('/admin/tag/list.php'); ?>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Form Tambah Tag</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/admin" class="breadcrumb-link">Admin</a></li>
                                        <li class="breadcrumb-item"><a href="/admin/tag/tambah.php" class="breadcrumb-link">Tambah Tag</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-8">
                        <div class="card">
                            <div class="card-header d-flex">
                                <h4 class="card-header-title">Form Tag</h4>
                                <div class="toolbar ml-auto">
                                    <button type="submit" id="save-btn" form="main-form" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form id="main-form" action="<?=$_SERVER['PHP_SELF']?>" method='POST' enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-form-label">Nama</label>
                                        <input id="nama" name="nama" type="text" class="form-control">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/foot.snip.html'; ?>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/scripts.snip.html'; ?>
</body>
 
</html>