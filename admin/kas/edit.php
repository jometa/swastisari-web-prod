<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');

    // Handle form data if POST
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        // Set default picture for cabang.
        $picture_url = '/uploads/default-cabang.jpg';
        $upload = false;

        if ($_FILES['picture']['size'] != 0) {

            // Get extension
            $name = $_FILES["picture"]["name"];
            $ext = end((explode(".", $name)));

            // Generate random name
            $randname = uniqid() . '.' . $ext;


            $path = $_SERVER['DOCUMENT_ROOT'] . "/uploads/";
            $path = $path . $randname;

            if(file_exists($path)) {
                chmod($path,0755); //Change the file permissions if allowed
                unlink($path); //remove the file
            }

            if(move_uploaded_file($_FILES['picture']['tmp_name'], $path)) {
              echo "The file ".  basename( $_FILES['picture']['name']) . " has been uploaded";
            } else{
                exit('Gagal mengupload file');
            }

            $picture_url = '/uploads/' . $randname;
            $upload = true;
        }

        $query_str = "
            UPDATE kantor_kas
                SET
                    nama = '$_POST[nama]',
                    alamat = '$_POST[alamat]',
                    no_tlpn = '$_POST[no_tlpn]',
                    id_kantor_cbg = $_POST[jmlh_anggota]
                WHERE id = $_GET[id];
        ";

        $db->begin_transaction();
        $update_result = $db->query($query_str);
        if ($upload) {
            $update_gambar = "
                UPDATE kantor_kas
                SET
                    picture = '$picture_url'
                WHERE id = $_GET[id];
            ";
            $db->query($update_gambar);
        }
        $update_result = $db->commit();

        if ($update_result) { ?>
            <script>
                window.location = '/admin/kas/list.php';
            </script>
        <?php }
        else {
            echo $query_str;
            exit('Gagal mengupload data');
        }
    }

    // Pull the data if GET.
    $id = $_GET['id'];
    $query_result = $db->query("SELECT * FROM kantor_kas WHERE id = $id");
    if (!$query_result) {
        exit('Gagal mengambil data');
    }
    $item = $query_result->fetch_assoc();
?>

<?php 
    require($_SERVER['DOCUMENT_ROOT'] . '/admin/templates/layout.php');
?>

<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/head.snip.html'; ?>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/dash-head.snip.html'; ?>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <?php templ_left_nav('/admin/cabang'); ?>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Form Edit Kantor Kas</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/admin" class="breadcrumb-link">Admin</a></li>
                                        <li class="breadcrumb-item"><a href="/admin/kas/tambah" class="breadcrumb-link">Edit Kantor Kas</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-8">
                        <div class="card">
                            <div class="card-header d-flex">
                                <h4 class="card-header-title">Form Kantor Kas</h4>
                                <div class="toolbar ml-auto">
                                    <button type="submit" form="main-form" href="/admin/cabang/tambah" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form id="main-form"
                                    action="<?= $_SERVER['PHP_SELF']. '?id=' . $id ?>"
                                    method='POST'
                                    enctype="multipart/form-data"
                                >
                                  <div class="form-group">
                                      <label for="input-select">Pilih Kantor Cabang</label>
                                      <select class="form-control" name="id_kantor_cbg">
                                          <?php 
                                              $query_result = $db->query("SELECT * FROM kantor_cbg");
                                              if (!$query_result) {
                                                  exit('Gagal mengambil kantor cabang');
                                              }
                                              $cabang_items = $query_result->fetch_all(MYSQLI_ASSOC);

                                              foreach ($cabang_items as $cabang)  { 
                                                $selected = $cabang['id'] == $item['id_kantor_cbg'] ? 'selected' : '';
                                                ?>
                                                  <option value="<?= $cabang['id'] ?>" <?= $selected ?>> <?= $cabang['nama'] ?> </option>
                                              <?php }
                                          ?>
                                      </select>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-form-label">Nama Kantor Kas</label>
                                      <input name="nama" type="text" class="form-control" value="<?= $item['nama'] ?>">
                                  </div>
                                  <div class="form-group">
                                      <label class="col-form-label">Alamat</label>
                                      <input name="alamat" type="text" class="form-control" value="<?= $item['alamat'] ?>">
                                  </div>
                                  <div class="form-group">
                                      <label class="col-form-label">No. Telpon</label>
                                      <input name="no_tlpn" type="text" class="form-control" value="<?= $item['no_tlpn'] ?>">
                                  </div>
                                  <div class="form-group">
                                      <label class="col-form-label">Foto</label>
                                      <div class="custom-file mb-3">
                                          <input type="file" class="custom-file-input" name="picture">
                                          <label class="custom-file-label">File Input</label>
                                      </div>
                                  </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/foot.snip.html'; ?>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/scripts.snip.html'; ?>
</body>
 
</html>