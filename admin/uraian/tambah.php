<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');


    // Handle form data
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $insert_str = "
            INSERT INTO uraian
                (tipe, nama)
            VALUES
                ('$_POST[tipe]', '$_POST[nama]')
        ";
        $insert_result = $db->query($insert_str);
        
        if ($insert_result) { ?>
            <script>
                window.location = '/admin/uraian/list.php';
            </script> <?php 
        } else {
            echo '<br/>';
            echo $insert_str;
            exit('Gagal mengupload data uraian');
        }
    }
?>

<?php 
    require($_SERVER['DOCUMENT_ROOT'] . '/admin/templates/layout.php');
?>

<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/head.snip.html'; ?>
    <link href="/admin/assets/libs/css/quill.snow.css" rel="stylesheet"/>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/dash-head.snip.html'; ?>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <?php templ_left_nav('/admin/cabang'); ?>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Form Tambah Uraian</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/admin" class="breadcrumb-link">Admin</a></li>
                                        <li class="breadcrumb-item"><a href="/admin/berita/tambah.php" class="breadcrumb-link">Tambah Uraian</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-8">
                        <div class="card" id="vue-app">
                            <div class="card-header d-flex">
                                <h4 class="card-header-title">Form Uraian</h4>
                                <div class="toolbar ml-auto">
                                    <button type="button"
                                        id="save-btn"
                                        class="btn btn-primary"
                                        v-on:click="handleSave"
                                    >Simpan</button>
                                </div>
                            </div>
                            <div class="card-body">

                                <form>

                                    <div class="form-group">
                                        <label for="input-select">Tipe Uraian</label>
                                        <select class="form-control" id="tipe-uraian" name="tipe" v-model="item.tipe">
                                            <option v-for="(opt, idx) in optionsTipe" :key="idx" :value="opt.value">{{ opt.text }}</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label">Nama</label>
                                        <input type="text" class="form-control" name="nama" v-model="item.nama">
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn" v-on:click="handleAddDetail">Tambah Detail Tahun</button>
                                    </div>

                                    <div class="form-group">
                                        <div class="row py-2" v-for="(dtahun, idx) in item.detail" :key="idx">
                                            <div class="col-sm-4">
                                                <input type="number" min="2016" class="form-control" v-model="dtahun.tahun">
                                            </div>

                                            <div class="col-sm-4">
                                                <currency-input
                                                    v-model="dtahun.nominal"
                                                    class="form-control"
                                                    :distraction-free="distractionFree"
                                                    :currency="currency"
                                                >
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/foot.snip.html'; ?>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/scripts.snip.html'; ?>
    <script src="/js/vue.js"></script>
    <script src="/js/vue-currency-input.umd.js"></script>
    <script src="/js/axios.min.js"></script>
    <script>
        var app = new Vue({
            el: '#vue-app',
            data: {
                item: {
                    tipe: 'MODAL',
                    nama: '',
                    detail: []
                },
                optionsTipe: [
                    { text: 'Modal', value: 'MODAL' },
                    { text: 'Pinjaman', value: 'PINJAMAN' }
                ],
                currency: {
                    prefix: 'RP '
                }
            },
            computed: {
                distractionFree () {
                  return {
                    hideNegligibleDecimalDigits: this.hideNegligibleDecimalDigits,
                    hideCurrencySymbol: this.hideCurrencySymbol,
                    hideGroupingSymbol: this.hideGroupingSymbol
                  }
                }
            },

            methods: {
                handleAddDetail() {
                    var detail = this.item.detail;
                    var n = detail.length;
                    var lastYear = n > 0 ? detail[n - 1].tahun : 2016;
                    this.item.detail.push({
                        tahun: lastYear + 1,
                        nominal: 0
                    });
                },

                handleSave() {
                    var detail = this.item.detail.length == 0 ? [] : this.item.detail;
                    var payload = {
                        ...this.item,
                        detail: JSON.stringify(detail)
                    };
                    axios.post('/admin/uraian/api-tambah.php', payload)
                        .then(resp => {
                            console.log(resp);
                            window.location = '/admin/uraian/list.php';
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }
            }
        });
    </script>
</body>
 
</html>