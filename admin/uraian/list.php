<?php 
    require($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
    require($_SERVER['DOCUMENT_ROOT'] . '/admin/templates/layout.php');
?>

<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/head.snip.html'; ?>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/dash-head.snip.html'; ?>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <?php templ_left_nav('/admin/berita/list.php'); ?>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Daftar Uraian</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/admin" class="breadcrumb-link">Admin</a></li>
                                        <li class="breadcrumb-item"><a href="/admin/berita/list.php" class="breadcrumb-link">List Uraian</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12-5 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-header d-flex">
                                <h4 class="card-header-title">Uraian</h4>
                                <div class="toolbar ml-auto">
                                    <a href="/admin/uraian/tambah.php" class="btn btn-primary btn-sm ">Tambah</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col">Tipe</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $query_result = $db->query("SELECT * FROM uraian");
                                            if (!$query_result) {
                                                exit('Gagal mengambil data');
                                            }

                                            $items = $query_result->fetch_all(MYSQLI_ASSOC);
                                            foreach ($items as $item) { ?>
                                                <tr>
                                                    <td><?= $item['id'] ?></td>
                                                    <td><?= $item['tipe'] ?></td>
                                                    
                                                    <td><?= $item['nama'] ?></td>

                                                    <td>
                                                        <?php
                                                            $konten = json_decode($item['konten']);
                                                            foreach ($konten as $detail) { ?>
                                                                    <div class="d-flex justify-content-between align-items-center px-2 py-2"
                                                                        style="border-bottom: 1px solid #ddd;"
                                                                    >
                                                                        <div>
                                                                            <?= $detail->tahun ?>
                                                                        </div>
                                                                        <div>
                                                                            <?= $detail->nominal ?>
                                                                        </div>
                                                                    </div>
                                                            <?php }
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <div class="dropdown show">
                                                          <a class="btn btn-sm btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fas fa-fw fa-th"></i>
                                                          </a>

                                                          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                            <a class="dropdown-item"
                                                                href="<?= '/admin/uraian/edit.php?id=' . $item['id'] ?>">
                                                                Edit
                                                            </a>
                                                            <a class="dropdown-item"
                                                                href="<?= '/admin/uraian/hapus.php?id=' . $item['id'] ?>"
                                                            >
                                                                Hapus
                                                            </a>
                                                          </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/foot.snip.html'; ?>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/scripts.snip.html'; ?>
</body>
 
</html>