<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');

    $id = $_GET['id'];

    // Handle form data
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {


        // Set default picture for cabang.
        $picture_url = '/uploads/default-cabang.jpg';
        $upload = false;

        if ($_FILES['foto']['size'] != 0) {

            // Get extension
            $name = $_FILES["foto"]["name"];
            $ext = end((explode(".", $name)));

            // Generate random name
            $randname = uniqid() . '.' . $ext;


            $path = $_SERVER['DOCUMENT_ROOT'] . "/uploads/";
            $path = $path . $randname;

            echo $path;

            if(move_uploaded_file($_FILES['foto']['tmp_name'], $path)) {
              echo "The file ".  basename( $_FILES['foto']['name']) . " has been uploaded";
            } else{
                exit('Gagal mengupload file');
            }

            $picture_url = '/uploads/' . $randname;
        }

        $konten = $db->real_escape_string($_POST['konten']);
        $id = $_GET['id'];
        $insert_str = "
            UPDATE berita SET
                judul = '$_POST[judul]',
                konten = '$konten'
            WHERE id = $id
        ";
        if ($upload) {
            $insert_str = $insert_str . "
                UPDATE berita
                SET
                    berita = '$picture_url'
                WHERE id = $id;
            ";
        }
        # BEGIN TRANS
        $db->begin_transaction();

        $insert_result = $db->query($insert_str);
        $berita_id = $id;

        $tags = $_POST['tag'];

        $tag_values = array_map(function ($tag) use ($berita_id) {
            return "($berita_id, $tag)";
        }, $tags);
        $tag_values = implode(" , ", $tag_values);

        $delete_tag = "DELETE FROM berita_tag WHERE berita_tag.id_berita = $id; ";
        $insert_tag_str = "INSERT INTO berita_tag (id_berita, id_tag) VALUES " . $tag_values;
        $tag_query_str = $delete_tag . $insert_tag_str;

        $db->query($delete_tag);
        $db->query($insert_tag_str);
        // $insert_tag_result = $db->query($tag_query_str);
        $result = $db->commit();

        if ($result) { ?>
          <script>
              window.location = '/admin/berita/list.php';
          </script> <?php 
      }

        if ($insert_result) { 
            if ($insert_tag_result) { ?>
                <script>
                    window.location = '/admin/berita/list.php';
                </script> <?php 
            } else {
                echo $tag_query_str;
                exit('Gagal mengupload data tag');
            }
        } else {
            echo '<br/>';
            echo $insert_str;
            exit('Gagal mengupload data berita');
        }
    }

    $query_result = $db->query("SELECT * FROM berita WHERE id = $id");
    if (!$query_result) {
        exit('Gagal menambil data berita');
    }

    $item = $query_result->fetch_assoc();
    $konten = $item['konten'];
?>

<?php 
    require($_SERVER['DOCUMENT_ROOT'] . '/admin/templates/layout.php');
?>

<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/head.snip.html'; ?>
    <link href="/admin/assets/libs/css/quill.snow.css" rel="stylesheet"/>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/dash-head.snip.html'; ?>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <?php templ_left_nav('/admin/cabang'); ?>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Form Tambah Berita</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/admin" class="breadcrumb-link">Admin</a></li>
                                        <li class="breadcrumb-item"><a href="/admin/berita/tambah.php" class="breadcrumb-link">Tambah Berita</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-8">
                        <div class="card">
                            <div class="card-header d-flex">
                                <h4 class="card-header-title">Form Berita</h4>
                                <div class="toolbar ml-auto">
                                    <button type="button" id="save-btn" form="main-form" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form id="main-form"
                                    action="<?=$_SERVER['PHP_SELF']?>?id=<?= $id ?>"
                                    method='POST'
                                    enctype="multipart/form-data"
                                >
                                    <div class="form-group">
                                        <label class="col-form-label">Judul</label>
                                        <input id="nama" name="judul" type="text" class="form-control" value="<?= $item['judul'] ?>">
                                    </div>

                                    
                                    <textarea name="konten" id="hidden-content" style="display: none;"></textarea>

                                    <div id="editor">
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label">Foto</label>
                                        <div class="custom-file mb-3">
                                            <input type="file" class="custom-file-input" name="foto">
                                            <label class="custom-file-label">File Input</label>
                                        </div>
                                    </div>

                                    <?php 
                                        $query_tag = $db->query("SELECT * FROM tag");
                                        if (!$query_tag) {
                                            exit("gagal mengambil data tags");
                                        }

                                        $tags = $query_tag->fetch_all(MYSQLI_ASSOC);

                                        $query_tag_bt = $db->query("SELECT t.id, t.nama FROM 
                                            tag t
                                            LEFT JOIN berita_tag bt ON bt.id_tag = t.id
                                            LEFT JOIN berita b ON b.id = bt.id_berita
                                            WHERE bt.id_berita = $item[id]");
                                        if (!$query_tag_bt) {
                                            exit("gagal mengambil data tags");
                                        }

                                        $selected_tags = $query_tag_bt->fetch_all(MYSQLI_ASSOC);
                                        $selected_tags = array_map(function ($t) {
                                            return $t['id'];
                                        }, $selected_tags);

                                        foreach ($tags as $tag) { ?>
                                            <label class="custom-control custom-checkbox">
                                                <input
                                                    name="tag[]"
                                                    value="<?= $tag['id'] ?>" type="checkbox"
                                                    <?php if (in_array($tag['id'], $selected_tags)) { ?> 
                                                        checked
                                                    <?php } ?>
                                                    class="custom-control-input"><span class="custom-control-label"><?= $tag['nama'] ?></span>
                                            </label>
                                    <?php } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/scripts.snip.html'; ?>
    <script src="/admin/assets/libs/js/quill.js"></script>
    <script src="/admin/assets/libs/js/QuillDeltaToHtmlConverter.bundle.js"></script>
    <script>
      var quill = new Quill('#editor', {
        theme: 'snow'
      });

        var konten_raw = "<?= $db->real_escape_string($konten) ?>";
        konten_raw = JSON.parse(konten_raw);
        console.log(konten_raw);
        quill.setContents(konten_raw);

      $("#save-btn").click(function () {
        var delta = quill.getContents();
        var content = JSON.stringify(delta);

        // Get judul
        $("#hidden-content").val(content);
        setTimeout(function () {
            $("#main-form").submit();
        }, 1000);
      });
    </script>
</body>
 
</html>