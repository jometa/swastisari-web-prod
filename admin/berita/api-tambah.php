<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');

    // Handle form data
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {


        // Set default picture for cabang.
        $picture_url = '/uploads/default-cabang.jpg';

        if(!empty($_FILES['foto'])) {

            // Get extension
            $name = $_FILES["foto"]["name"];
            $ext = end((explode(".", $name)));

            // Generate random name
            $randname = uniqid() . '.' . $ext;


            $path = $_SERVER['DOCUMENT_ROOT'] . "/uploads/";
            $path = $path . $randname;

            echo $path;

            if(move_uploaded_file($_FILES['foto']['tmp_name'], $path)) {
              echo "The file ".  basename( $_FILES['foto']['name']) . " has been uploaded";
            } else{
                exit('Gagal mengupload file');
            }

            $picture_url = '/uploads/' . $randname;
        }

        $insert_result = $db->query("
            INSERT INTO berita
                (nama, alamat, email, no_tlpn, picture)
            VALUES
                ('$_POST[nama]', '$_POST[alamat]', '$_POST[email]', '$_POST[no_tlpn]', '$picture_url')
        ");
        $berita_id = $db->insert_id;

        $tags = $_POST['tag'];
        $tag_values = array_map(function ($tag) use ($berita_id) {
            return "($berita_id, $tag)"
        }, $tags);
        $tag_values = implode(" , ", $tag_values);

        $insert_tag_str = "INSERT INTO berita_tag (id_berita, id_tag) " . $tag_values;

        $insert_tag_result = $db->query($insert_tag_str);

        if ($insert_result && $insert_tag_result) {
            echo 'OK';
        } else {
            http_response_code(500);
            echo 'FAIL';
        }
    }
?>