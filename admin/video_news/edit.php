<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');

    $id = $_GET['id'];

    // Handle form data
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $insert_str = "
            UPDATE video_news
                SET video_id = '$_POST[video_id]', 
                  video_title = '$_POST[video_title]'
            WHERE id = $id
        ";

        $db->begin_transaction();
        $db->query($insert_str);
        $tags = $_POST['tag'];

        $tag_values = array_map(function ($tag) use ($id) {
            return "($id, $tag)";
        }, $tags);
        $tag_values = implode(" , ", $tag_values);

        $delete_tag_str = "DELETE FROM video_news_tag WHERE id_video_news = $id";
        $insert_tag_str = "INSERT INTO video_news_tag (id_video_news, id_tag) VALUES " . $tag_values;
        $db->query($delete_tag_str);
        $db->query($insert_tag_str);
        $result = $db->commit();

        if ($result) { ?>
            <script>
                window.location = '/admin/video_news/list.php';
            </script> 
        <?php } else {
                echo $insert_str;
                exit('Gagal mengupload data tag');
        }
    }
    $query_result = $db->query("SELECT * FROM video_news WHERE id = $id");
    if (!$query_result) {
        exit('Gagal menambil data berita');
    }

    $item = $query_result->fetch_assoc();
    $konten = $item['konten'];
?>

<?php 
    require($_SERVER['DOCUMENT_ROOT'] . '/admin/templates/layout.php');
?>

<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/head.snip.html'; ?>
    <link href="/admin/assets/libs/css/quill.snow.css" rel="stylesheet"/>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/dash-head.snip.html'; ?>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <?php templ_left_nav('/admin/tag/list.php'); ?>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Form Tambah Berita Video</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/admin" class="breadcrumb-link">Admin</a></li>
                                        <li class="breadcrumb-item"><a href="/admin/video_news/tambah.php" class="breadcrumb-link">Tambah Video News</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-8">
                        <div class="card">
                            <div class="card-header d-flex">
                                <h4 class="card-header-title">Form Video News</h4>
                                <div class="toolbar ml-auto">
                                    <button type="submit" id="save-btn" form="main-form" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form id="main-form" 
                                  action="<?=$_SERVER['PHP_SELF']?>?id=<?= $id ?>" 
                                  method='POST' 
                                  enctype="multipart/form-data"
                                >
                                    <div class="form-group">
                                        <label class="col-form-label">Video ID</label>
                                        <input id="video_id" name="video_id" type="text" class="form-control" value="<?= $item['video_id'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Video Title</label>
                                        <input id="video_title" name="video_title" type="text" class="form-control" value="<?= $item['video_title'] ?>">
                                    </div>
                                    <?php 
                                        $query_tag = $db->query("SELECT * FROM tag");
                                        if (!$query_tag) {
                                            exit("gagal mengambil data tags");
                                        }

                                        $tags = $query_tag->fetch_all(MYSQLI_ASSOC);

                                        $query_tag_bt = $db->query("SELECT t.id, t.nama FROM 
                                            tag t
                                            LEFT JOIN video_news_tag bt ON bt.id_tag = t.id
                                            LEFT JOIN video_news b ON b.id = bt.id_video_news
                                            WHERE bt.id_video_news = $item[id]");
                                        if (!$query_tag_bt) {
                                            exit("gagal mengambil data tags");
                                        }

                                        $selected_tags = $query_tag_bt->fetch_all(MYSQLI_ASSOC);
                                        $selected_tags = array_map(function ($t) {
                                            return $t['id'];
                                        }, $selected_tags);

                                        foreach ($tags as $tag) { ?>
                                            <label class="custom-control custom-checkbox">
                                                <input
                                                    name="tag[]"
                                                    value="<?= $tag['id'] ?>" type="checkbox"
                                                    <?php if (in_array($tag['id'], $selected_tags)) { ?> 
                                                        checked
                                                    <?php } ?>
                                                    class="custom-control-input"><span class="custom-control-label"><?= $tag['nama'] ?></span>
                                            </label>
                                    <?php } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/foot.snip.html'; ?>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/scripts.snip.html'; ?>
</body>
 
</html>