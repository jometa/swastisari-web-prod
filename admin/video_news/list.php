<?php 
    require($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
    require($_SERVER['DOCUMENT_ROOT'] . '/admin/templates/layout.php');
?>

<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/head.snip.html'; ?>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/dash-head.snip.html'; ?>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <?php templ_left_nav('/admin/berita/list.php'); ?>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Daftar Video News</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/admin" class="breadcrumb-link">Admin</a></li>
                                        <li class="breadcrumb-item"><a href="/admin/video_news/list.php" class="breadcrumb-link">List Berita</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12-5 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-header d-flex">
                                <h4 class="card-header-title">Video</h4>
                                <div class="toolbar ml-auto">
                                    <a href="/admin/video_news/tambah.php" class="btn btn-primary btn-sm ">Tambah</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col">ID</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Tag</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $query_result = $db->query("SELECT * FROM video_news");
                                            if (!$query_result) {
                                                exit('Gagal mengambil data');
                                            }

                                            $items = $query_result->fetch_all(MYSQLI_ASSOC);
                                            foreach ($items as $item) {

                                                $query_tag = $db->query("
                                                        SELECT t.id, t.nama
                                                            FROM video_news v
                                                            LEFT JOIN video_news_tag vt ON v.id = vt.id_video_news
                                                            LEFT JOIN tag t ON t.id = vt.id_tag
                                                            WHERE v.id = $item[id]");
                                                if (!$query_tag) {
                                                    exit('Gagal mengambil data');
                                                }
                                                $tags = $query_tag->fetch_all(MYSQLI_ASSOC);
                                                ?>

                                                <tr>
                                                    <td>
                                                        <div class="m-r-10">
                                                            <img 
                                                                src="https://img.youtube.com/vi/<?=$item['video_id']?>/default.jpg"
                                                                alt="user"
                                                                width="68">
                                                        </div>
                                                    </td>
                                                    <td><?= $item['video_id'] ?></td>
                                                    <td><?= $item['video_title'] ?></td>
                                                    <td>
                                                    <?php
                                                        foreach ($tags as $tag) { ?>
                                                            <span class="badge badge-primary"><?= $tag['nama'] ?></span>
                                                        <?php }
                                                    ?>
                                                    </td>
                                                    <td>
                                                        <div class="dropdown show">
                                                          <a class="btn btn-sm btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fas fa-fw fa-th"></i>
                                                          </a>

                                                          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                            <a class="dropdown-item"
                                                                href="<?= '/admin/video_news/edit.php?id=' . $item['id'] ?>"
                                                            >
                                                                Edit
                                                            </a>
                                                            <a class="dropdown-item"
                                                                href="<?= '/admin/video_news/hapus.php?id=' . $item['id'] ?>"
                                                            >
                                                                Hapus
                                                            </a>
                                                          </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/foot.snip.html'; ?>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/commons/scripts.snip.html'; ?>
</body>
 
</html>