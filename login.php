<?php 
    require_once($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
    session_start();

    $username_error = false;
    $password_error = false;

    $message = '';
    $username = '';
    $password = '';

    while (true) {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $query_str = "SELECT * FROM users WHERE username = '$username'";
            $query_result = $db->query($query_str);
            // var_dump($query_str);
            // var_dump($query_result);
            // die();

            if (!$query_result || $query_result->num_rows != 1) {
                die('here');
                $username_error = true;
                $message = 'Check Kembali username anda';
                break;
            }

            // var_dump(!$query_result || $query_result->num_rows != 1);
            $item = $query_result->fetch_assoc();
            $password_hash = $item['password'];
            if (password_verify($password, $password_hash)) {
                $_SESSION['swastisari.username'] = $username;
                header('Location: /admin');
                die();    
            } else {
                $password_error = true;
                $message = 'Password tidak cocok';
                break;
            }
            
        }
        break;
    }
    // var_dump($username_error);
    // var_dump($password_error);
    // die();

?>
<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/admin/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="/admin/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/admin/assets/libs/css/style.css">
    <link rel="stylesheet" href="/admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center"><a href="/"><img class="logo-img" src="/admin/assets/images/logo.png" alt="logo"></a><span class="splash-description">Please enter your user information.</span></div>
            <div class="card-body">
                <form method='POST' enctype="multipart/form-data">
                    <div class="form-group">
                        <?php
                            $username_class = 'form-control form-control-lg';
                            if ($username_error) {
                                $username_class = $username_class . ' is-invalid';
                            }
                        ?>
                        <input
                            class="<?= $username_class ?>"
                            name="username"
                            type="text"
                            placeholder="Username"
                            value="<?= $username ?>"
                        >
                        <?php 
                            if ($username_error) { ?>
                                <div class="invalid-feedback">
                                    <?= $message ?>
                                </div>
                            <?php }
                        ?>
                    </div>

                    <div class="form-group">
                        <?php
                            $password_class = 'form-control form-control-lg';
                            if ($password_error) {
                                $password_class = $password_class . ' is-invalid';
                            }
                        ?>
                        <input
                            class="<?= $password_class ?>"
                            name="password" type="password"
                            placeholder="Password"
                            value="<?= $password ?>"
                        >
                        <?php 
                            if ($password_error) { ?>
                                <div class="invalid-feedback">
                                    <?= $message ?>
                                </div>
                            <?php }
                        ?>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
                </form>
            </div>
        </div>
    </div>
  
    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="/admin/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="/admin/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
</body>
 
</html>