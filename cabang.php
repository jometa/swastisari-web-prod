<?php
    require($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/head.html');
    ?>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    
    <!-- ##### Header Area End ##### -->
    <?php
    include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/header.html');
    ?>

    <!-- ##### Breadcrumb Area Start ##### -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(/mag/img/bg-img/41.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>DAFTAR KANTOR CABANG</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="mag-breadcrumb py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/"><i class="fa fa-home" aria-hidden="true"></i> Beranda</a></li>
                            <li class="breadcrumb-item"><a href="#">Feature</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Archive by Category “TRAVEL”</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <!-- ##### Archive Post Area Start ##### -->
    <div class="archive-post-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-xl-8">
                    <div class="archive-posts-area bg-white p-30 mb-30 box-shadow">

                        <?php
                        $take = 5;
                        if (isset($_GET['take'])) {
                            $take = (int)$_GET['take'];
                        }

                        $query_result = $db->query("SELECT * FROM kantor_cbg LIMIT $take");
                        if (!$query_result) {
                            exit('error load data');
                        }
                        $items = $query_result->fetch_all(MYSQLI_ASSOC);
                        foreach ($items as $item) { ?>

                            <div class="single-catagory-post d-flex flex-wrap">
                                <!-- Thumbnail -->
                                <div class="post-thumbnail bg-img" style="background-image: url(<?= $item['picture'] ?>);">

                                </div>

                                <!-- Post Contetnt -->
                                <div class="post-content">
                                    <a href="/cabang-detail.php?id=<?= $item['id'] ?>" class="post-title">Kantor Cabang <?= $item['nama'] ?></a>
                                    <!-- Post Meta -->
                                    <div class="post-meta-2">
                                        <div><h5>Alamat</h5> <?= $item['alamat'] ?></div>
                                        <div><h5>Email</h5> <?= $item['email'] ?></div>
                                        <div><h5>No. Telpon</h5> <?= $item['no_tlpn'] ?></div>
                                    </div>
                                    
                                </div>
                            </div>

                        <?php }
                        ?>

                        <!-- Pagination -->
                       <!--  <nav>
                            <ul class="pagination">
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#"><i class="ti-angle-right"></i></a></li>
                            </ul>
                        </nav> -->
                        <a 
                            href="<?=$_SERVER['PHP_SELF']?>?take=<?= $take + 5 ?>"
                            class="btn btn-lg btn-info btn-block"
                        >
                            Next
                        </a>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                    <div class="sidebar-area bg-white mb-30 box-shadow">
                        <?php
                        include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/right-side-overview.php');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ##### Footer Area End ##### -->
    <?php
    include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/footer.html');
    ?>

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <?php
    include($_SERVER['DOCUMENT_ROOT'] . '/common-snippets/scripts.html');
    ?>
</body>

</html>