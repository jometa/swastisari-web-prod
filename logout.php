<?php 
    require_once($_SERVER['DOCUMENT_ROOT'] . '/koneksi.php');
    session_start();
    session_destroy();
    header('Location: /login.php');
    die();
?>